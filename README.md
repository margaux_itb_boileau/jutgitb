# Jutge ITB
## Funcionament
En iniciar el programa es crearà un usuari per defecte del sistema (nom: jutge, clau: jutge) pots iniciar sessió amb aquest o registrar-te com a estudiant nou. Si ets professor podràs accedir a les funcionalitats corresponents introduïnt la clau necessària. 
Com a estudiant podràs resoldre problemes linealment segons l'itinerari d'aprenentatge o escollir el que desitgis, també podràs veure el teu historial de problemes amb el nombre d'intents i estat corresponents. 
Com a professor podràs veure els estudiants del sistema, afegir nous problemes i generar un informe de notes de les estudiants.  
Per a navegar hauràs de contestar preguntes de (s/n) o de multiple opció introduïnt el nombre de l'acció desitjada. 
ATENCIÓ: Abans de sortir el programa es guarden tots els canvis realitzats, en cas de d'aturar el programa incorrectament (no a través de l'opció del menu) es perdrà tot el progrés

## Instal·lació
NOTA: Per a poder utilitzar el programa cal tenir instalat postgresql.
1. Clona el projecte a la teva màquina local.
2. Còpia i enganxa el contingut del fitxer "script.sql".
3. Executa l'arxiu "main.kt" al IDE per a començar a utilitzar-lo.
