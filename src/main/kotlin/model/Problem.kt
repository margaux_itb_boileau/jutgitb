package model

data class Problem( var idProblema:Int, var idUser:Int, val titol:String, val enunciat:String, val jocPubl:JocProves, val jocPriv:JocProves, val tipus:String, var intents:Int=0, var estat:Boolean = false) {
    fun corregeix(resposta: String) {
        intents++
        estat = (resposta == jocPriv.ouput)
    }
}