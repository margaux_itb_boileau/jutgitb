import model.JocProves
import model.Problem
import model.User
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.*
import kotlin.system.exitProcess

var connection: Connection? = null
var user:User? = null
val scanner = Scanner(System.`in`)
var users = mutableListOf<User>()
var userProblems = mutableListOf<Problem>()
var allProblems = mutableListOf<Problem>()

fun main() {

    val jdbcUrl = "jdbc:postgresql://localhost:5432/jutgdb"

    try {
        connection = DriverManager.getConnection(jdbcUrl, "postgres", "postgres")
    } catch (e: SQLException) {
        println("Error ${e.errorCode} al hacer la conexión a la tabla: ${e.message}")
    }

    getAllUsers()
    getAllProblems()

    println("Benvingut a jutgITB")
    printAjuda()

    println("Quin rol tens?\n" +
            "1. Professor\n" +
            "2. Estudiant\n")
    var action= scanner.nextLine()

    if (action=="1") {
        do {
            println("Introdueix la clau per a accedir a les funcionalitats de professor:")
            val clau= scanner.nextLine()
            if (clau=="itb") {
                menuProfessor()
            }
            else {
                println("Clau incorrecte:\n" +
                        "1. Tornar a intentar\n" +
                        "2. Entrar com a estudiant\n")
                action = scanner.nextLine()
            }
        } while (action=="1")
    }

    if (action == "2"){
        while(user == null) {
            println("Identifica't:\n" +
                    "1. Inicia sessió\n" +
                    "2. Registrar-se\n")
            action = scanner.nextLine()

            println("Nom:")
            val name= scanner.nextLine()
            println("Contrasenya:")
            val password= scanner.nextLine()

            if (action == "1") login(name, password)
            else if (action == "2") insertUser(name, password)

            if (user == null) println("Credencials incorrectes")
        }
        getUserProblems(user!!.id)
        menuStudent()
    }
}

//DATA
fun login(name:String, password:String) {
    for (i in users) {
        if(name == i.name && password == i.password) user = i
    }
}
fun getAllUsers(){
    users.clear()
    val query = "SELECT * FROM users"

    try {
        val statement = connection!!.createStatement()
        val resultAlumnos = statement.executeQuery(query)

        while(resultAlumnos.next()){
            val id = resultAlumnos.getInt("iduser")
            val name = resultAlumnos.getString("name")
            val password = resultAlumnos.getString("password")
            users.add(User(id, name, password))
        }
        statement.close()
    }catch (e: SQLException) {
        println("Error ${e.errorCode}: ${e.message}")
    }
}
fun getAllProblems() {
    val query = "SELECT * FROM problems ORDER BY idproblem"

    try {
        val statement = connection!!.createStatement()
        val resultSet = statement.executeQuery(query)

        while(resultSet.next()){
            val idproblem  = resultSet.getInt("idproblem")
            val iduser  = resultSet.getInt("iduser")
            val title  = resultSet.getString("title")
            val body  = resultSet.getString("body")
            val publicinput  = resultSet.getString("publicinput")
            val publicoutput  = resultSet.getString("publicoutput")
            val privateinput  = resultSet.getString("privateinput")
            val privateoutput  = resultSet.getString("privateoutput")
            val type  = resultSet.getString("type")
            val tries  = resultSet.getInt("tries")
            val state  = resultSet.getString("state")

            val problem = Problem(idproblem, iduser,title,body, JocProves(publicinput,publicoutput), JocProves(privateinput,privateoutput), type)
            problem.estat = state.toBoolean()
            problem.intents = tries

            allProblems.add(problem)

        }
        statement.close()
    }catch (e: SQLException) {
        println("Error ${e.errorCode}: ${e.message}")
    }
    allProblems.sortBy { it.idProblema }
}
fun getUserProblems(id:Int){
    userProblems.clear()
    val query = "SELECT * FROM problems WHERE iduser=? ORDER BY idproblem"

    try {
        val statement = connection!!.prepareStatement(query)
        statement.setInt(1, id)
        val resultSet = statement.executeQuery()

        while(resultSet.next()) {
            val idproblem  = resultSet.getInt("idproblem")
            val iduser  = resultSet.getInt("iduser")
            val title  = resultSet.getString("title")
            val body  = resultSet.getString("body")
            val publicinput  = resultSet.getString("publicinput")
            val publicoutput  = resultSet.getString("publicoutput")
            val privateinput  = resultSet.getString("privateinput")
            val privateoutput  = resultSet.getString("privateoutput")
            val type  = resultSet.getString("type")
            val tries  = resultSet.getInt("tries")
            val state  = resultSet.getBoolean("state")

            val problem = Problem(idproblem,iduser,title,body, JocProves(publicinput,publicoutput), JocProves(privateinput,privateoutput), type, tries, state)

            userProblems.add(problem)
        }
        statement.close()
    }catch (e: SQLException) {
        println("Error ${e.errorCode}: ${e.message}")
    }
    userProblems.sortBy { it.idProblema }
}
fun insertUser(name:String, password:String) {
    val query = "INSERT INTO users (name, password) VALUES (?,?)"
    try {
        val statement = connection!!.prepareStatement(query)

        statement.setString(1, name)
        statement.setString(2, password)

        statement.executeUpdate()

        statement.close()
    } catch (e: SQLException) {
        println("Error ${e.errorCode}: ${e.message}")
    }
    getAllUsers()
    login(name,password)
    insertProblemsNewUser()}
fun insertProblemsNewUser() {
    getUserProblems(1)
    for (i in userProblems) {
        val query = "INSERT INTO problems (iduser, title, body, publicinput, publicoutput, privateinput, privateoutput, type, tries, state) VALUES (?,?,?,?,?,?,?,?,?,?)"
        try {
            val statement = connection!!.prepareStatement(query)

            statement.setInt(1, user!!.id)
            statement.setString(2, i.titol)
            statement.setString(3, i.enunciat)
            statement.setString(4, i.jocPubl.input)
            statement.setString(5, i.jocPubl.ouput)
            statement.setString(6, i.jocPriv.input)
            statement.setString(7, i.jocPriv.ouput)
            statement.setString(8, i.tipus)
            statement.setInt(9, 0)
            statement.setBoolean(10, false)
            statement.executeUpdate()

            statement.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
        }
    }
}
fun addNewProblemToLocalList(problem:Problem) {
    for (i in users) {
        problem.idUser = i.id
        allProblems.add(Problem(0,i.id, problem.titol, problem.enunciat, problem.jocPubl, problem.jocPriv, problem.tipus))
    }
}
fun updateProblems() {
    deleteAllProblems()

    for (i in allProblems) {

        val query = "INSERT INTO problems (idUser, title, body, publicinput, publicoutput, privateinput, privateoutput, type, tries, state) VALUES (?,?,?,?,?,?,?,?,?,?)"
        try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, i.idUser)
            statement.setString(2, i.titol)
            statement.setString(3, i.enunciat)
            statement.setString(4, i.jocPubl.input)
            statement.setString(5, i.jocPubl.ouput)
            statement.setString(6, i.jocPriv.input)
            statement.setString(7, i.jocPriv.ouput)
            statement.setString(8, i.tipus)
            statement.setInt(9, i.intents)
            statement.setBoolean(10, i.estat)
            statement.executeUpdate()

            statement.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
        }

    }
}
fun updateUserProblems() {
    for (i in userProblems) {

        val query = "UPDATE problems SET tries=?, state=? WHERE idproblem=?"
        try {
            val statement = connection!!.prepareStatement(query)
            statement.setInt(1, i.intents)
            statement.setBoolean(2, i.estat)
            statement.setInt(3, i.idProblema)
            statement.executeUpdate()

            statement.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode}: ${e.message}")
        }

    }
}
fun deleteAllProblems() {
    val query = "DELETE FROM problems"
    try {
        val statement = connection!!.prepareStatement(query)
        statement.executeUpdate()
        statement.close()
    } catch (e: SQLException) {
        println("Error ${e.errorCode}: ${e.message}")
    }
}

//ESTUDIANT
fun menuStudent() {
    do {
        println()
        println("Què vols fer?\n"+
                "1. Seguir amb l’itinerari d’aprenentatge\n" +
                "2. Veure i escollir del llistat de problemes\n" +
                "3. Consultar històric de problemes\n" +
                "4. Ajuda\n" +
                "5. Sortir\n")

        val instruccio = scanner.nextLine()
        when (instruccio) {
            "1"-> resume()
            "2"-> choose()
            "3"-> printHistorial()
            "4"-> printAjuda()
            "5"-> {
                updateUserProblems()
                exitProcess(0)
            }
        }
    }while (true)
}
fun resume() {
    for (problem in userProblems) {
        if (!problem.estat) {
            evalua(problem)
            println("Resoldre següent problema? (s/n)")
            val action = scanner.nextLine()
            if (action != "s") break
        }
    }
}
fun choose() {
    printList()
    myloop@do {
        println("Selecciona el problema segons el seu id")
        val index = scanner.nextLine().toInt()
        for (problem in userProblems) {
            if (problem.idProblema == index+userProblems[0].idProblema-1) {
                if (problem.estat) {
                    println("Problema ja resolt")
                }
                else {
                    evalua(problem)
                }
                println("Escollir un altre problema? (s/n)")
                val action = scanner.nextLine()
                if (action != "s") break@myloop
                break
            }
        }
    } while (true)

}
fun evalua(problema: Problem) {
    do {
        printProblem(problema)
        println("Entrada: ${problema.jocPriv.input}")
        println("Resposta:")
        val resposta = scanner.nextLine()
        problema.corregeix(resposta)
        if (problema.estat) {
            println("Resposta correcta")
            break
        }
        println("Resposta incorrecta, tornar a intentar? (s/n)")
        val action = scanner.nextLine()
    } while(action == "s")
}

//PROFESSOR
fun menuProfessor() {
    do {
        println("Què vols fer?\n" +
                "1. Veure llistat de problemes\n" +
                "2. Afegir un nou problema al llistat\n" +
                "3. Veure llistat d'estudiants\n" +
                "4. Generar un informe de les notes del estudiant\n" +
                "5. Ajuda\n"+
                "6. Sortir\n")
        val action = scanner.nextLine()
        when (action) {
            "1"-> {
                getUserProblems(1)
                printList()
            }
            "2"-> {
                val problem = createProblem()
                addNewProblemToLocalList(problem)
                updateProblems()
            }
            "3"-> printUsers()
            "4"-> {
                printUsers()
                println(createReport())
            }
            "5"-> printAjuda()
            "6"-> {
                updateUserProblems()
                exitProcess(0)
            }
        }
        println()
    } while (true)

}

//PRINTS
fun printUsers(){
    for (i in users) {
        println("Nom: ${i.name}\n" +
                "id d'estudiant: ${i.id}\n")
    }
}
fun printProblem(problem:Problem) {
    println(problem.titol)
    println(problem.enunciat)
    println("Entrada: ${problem.jocPubl.input}")
    println("Sortida: ${problem.jocPubl.ouput}")
}
fun printList() {
    for (i in userProblems.indices) {
        println("${i + 1}. ${userProblems[i].titol}")
    }
}
fun printHistorial() {
    for (i in userProblems.indices) {
        println("${i+1}. ${userProblems[i].titol}")
        println("\tIntents: ${userProblems[i].intents}")
        if (userProblems[i].estat) println("\tResolt: si")
        else println("\tResolt: no")
    }
}
fun createProblem(): Problem {
    println("Creació de nou problema:")
    val words = arrayListOf("Títol","Enunciat","Input public","Output public","Input privat","Output privat", "Secció")
    val elements = mutableListOf<String>()
    for (word in words) {
        println(word)
        val element = scanner.nextLine()
        elements.add(element)

    }
    val problema = Problem(0,0, elements[0], elements[1], JocProves(elements[2],elements[3]), JocProves(elements[4],elements[5]),elements[6],0,false)
    return problema
}
fun createReport(): String {
    println("Introdueix la id de l'estudiant per generar un informe de notes d'aquest")
    val id = scanner.nextInt()
    getUserProblems(id)

    var report = "┏━━━━━━━━━━┳━━━━━━━━━┳━━━━━━━━┓\n"
    report +="┃ PROBLEMA ┃ INTENTS ┃ RESOLT ┃\n"

    var fets = 0
    val notes = mutableListOf<Int>()
    for (i in userProblems.indices) {
        var color= "\u001B[33m"  //yellow
        var nota = 10
        var resolt = "no"
        if (userProblems[i].estat) {
            color = "\u001B[32m" //green
            resolt = "si"
            fets++
            when (userProblems[i].intents) {
                1 -> nota = nota
                2 -> nota -= 1
                3 -> nota -= 2
                4 -> nota -= 3
                5 -> nota -= 4
                else -> nota -= 5
            }
            notes.add(nota)
        }
        else if(userProblems[i].intents==0) color = "\u001B[31m" //red

        report+="┣━━━━━━━━━━╋━━━━━━━━━╋━━━━━━━━┫\n"
        report+="┃   $color${1}${"\u001B[0m"}     ┃    ${userProblems[i].intents}    ┃   $resolt   ┃\n"

    }
    report+="┗━━━━━━━━━━┻━━━━━━━━━┻━━━━━━━━┛\n"
    val mitjana = if (fets!=0) notes.sum()/fets
    else 0
    report+= "Nota mitjana de problemes resolts: $mitjana"
    return report
}
fun printAjuda() {
    println("INFORMACIÓ:\n" +
            "En iniciar el programa es crearà un usuari per defecte del sistema (nom: jutge, clau: jutge) pots iniciar sessió amb aquest o registrar-te com a estudiant nou. Si ets professor podràs accedir a les funcionalitats corresponents introduïnt la clau necessària.\n" +
            "Com a estudiant podràs resoldre problemes linealment segons l'itinerari d'aprenentatge o escollir el que desitgis, també podràs veure el teu historial de problemes amb el nombre d'intents i estat corresponents.\n" +
            "Com a professor podràs veure els estudiants del sistema, afegir nous problemes i generar un informe de notes dels estudiants. \n" +
            "Pera navegar hauràs de contestar preguntes de (s/n) o de multiple opció introduïnt el nombre de l'acció desitjada.\n" +
            "Atenció! Abans de sortir el programa es guarden tots els canvis realitzats, en cas de d'aturar el programa incorrectament (no a través de l'opció del menu) es perdrà tot el progrés.\n")
}