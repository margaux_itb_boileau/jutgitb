CREATE DATABASE jutgdb;

CREATE TABLE users (
    iduser SERIAL PRIMARY KEY,
    name VARCHAR(30),
    password VARCHAR(30)
);

CREATE TABLE problems (
    dproblem SERIAL PRIMARY KEY,
    iduser INT NOT NULL,
    title TEXT NOT NULL,
    body TEXT NOT NULL,
    publicinput VARCHAR(50) NOT NULL,
    publicoutput VARCHAR(50) NOT NULL,
    privateinput VARCHAR(50) NOT NULL,
    privateoutput VARCHAR(50) NOT NULL,
    type VARCHAR(30) NOT NULL,
    tries INT NOT NULL,
    state BOOLEAN NOT NULL,
    FOREIGN KEY (iduser) REFERENCES users(idUser)
);

INSERT INTO users (name, password) VALUES ('jutg', 'jutg');

INSERT INTO problems(iduser, title, body, publicinput, publicoutput, privateinput, privateoutput, type, tries, state) VALUES
    (1,'Calcula l''area', 'Descripcio:Una web d''habitatges de lloguer ens ha proposat una ampliacio. Volen mostrar l''area de les habitacions per llogar.Fes un programa que ens ajudi a calcular les dimensions d''una habitacio. Llegeix l''amplada i la llargada en metres (enters) i mostra''n l''area.Entrada: Per l''entrada sempre rebreu dos nombres enters que representen amplada i llargadaSortida: Per la sortida sempre haureu d''imprimir un altre enter amb l''area de l''habitacio que heu rebut', '3 5', '15', '12 12', '144', 'dades', 0, FALSE),
    (1,'Operacio boja', 'Descripcio:L''usuari escriu 4 enters i s''imprimeix el valor de sumar el primer amb el segon, multiplicat per el modul del tercer amb el quart.Entrada: 4 nombres enters introduits per l''usuariSortida: Un enter que representa el resultat de la seguent operacio (a + b) * (c % d)', '2 5 10 4', '14', '23 31 156 5', '54', 'dades', 0, FALSE),
    (1,'Calculadora de volum d''aire', 'Descripcio:Per poder fer un estudi de la ventilacio d''una aula necessitem poder calcular la quantitat d''aire que hi cap en una habitacio.Llegeix les 3 dimensions de l''aula i imprimeix per pantalla quin volum te.Entrada: Per l''entrada rebreu tres nombres decimals que representen la llargada, l''ample i l''alçada d''una habitacioSortida: Per la sortida sempre haureu d''imprimir un altre decimal amb el volum d''aire que cap en aquesta habitacio', '7.2 3.1 3.1', '69.192', '5.0 4.0 2.0', '40.0', 'dades', 0, FALSE),
    (1,'De Celsius a Fahrenheit', 'Descripcio:Feu un programa que rebi una temperatura en graus Celsius i la converteixi en graus FahrenheitEntrada: Per l''entrada rebreu un nombre decimal que representa una temperatura en graus CelsiusSortida: Per la sortida sempre haureu d''imprimir un altre decimal amb el valor de la temperatura en graus Fahrenheit', '33.1', '91.58', '-3.2', '26.2', 'dades', 0, FALSE),
    (1,'En rang', 'Descripcio:Escriu un programa que llegeixi 5 enters. El primer i el segon creen un rang, el tercer i el quart creen un altre rang.Mostra true si el cinque valor esta entre els dos rangs, si no false.Entrada: Per l''entrada rebreu cinc nombres entersSortida: Per la sortida sempre haureu d''imprimir true si el cinque enter esta dins del rang que formen el primer/segon i tercer/quart enter o false en el cas contrari', '10 40 20 30 25', 'true', '10 40 20 30 55', 'false', 'condicionals', 0, FALSE),
    (1,'Parell o senar?', 'Descripcio:Volem fer un programa que ens indiqui si un valor es parell o senar.Entrada: Per l''entrada rebreu un nombre enterSortida: El programa ha d''imprimir si el nombre es parell o senar', '5', 'senar', '24', 'parell', 'condicionals', 0, FALSE),
    (1,'Calcula la lletra del dni', 'Descripcio:Fes un programa que, donat un numero de dni, et calculi la lletra que li correspon.Entrada: Per l''entrada rebreu un nombre enter, que representa un numero de dni, es a dir, un nombre de 8 xifres exactesSortida: Per la sortida haureu d''imprimir el numero del dni amb la seva lletra corresponent', '65004204', '65004204V', '65004203', '65004203Q', 'condicionals', 0, FALSE),
    (1,'Quants dies te el mes', 'Descripcio:Fes un programa que, donat un numero de mes, indiqui quants dies te.Entrada: Per l''entrada rebreu un enter que representa un numero de mesSortida: Per la sortida haureu d''imprimir el nombre de dies que te aquest mes', '3', '31', '4', '30', 'condicionals', 0, FALSE),
    (1,'Imprimeix el rang', 'Descripcio:Escriu un programa que donat dos nombres enters imprimeixi la sequencia entre el primer i el segon, tenint en compte sempreque:primer ≤ segonEntrada: Per l''entrada rebreu dos nombres entersSortida: La sortida ha de ser una sequencia de numeros del primer fins al segon nombre introduit, imprimint-los tot en una mateixa linia', '3 14', '3,4,5,6,7,8,9,10,11,12,13,14', '5 17', '5,6,7,8,9,10,11,12,13,14,15,16,17', 'bucles', 0, FALSE),
    (1,'Eleva''l', 'Descripcio:Escriu un programa que donat dos nombres enters imprimeixi el resultat d''elevar el primer pel segon.(NO ES POT FERSERVIR EL MÈTODE QUE ENS PROPORCIONA MATH O KOTLIN)Entrada: Per l''entrada rebreu dos nombres entersSortida: La sortida ha de ser el resultat d''elevar el primer pel segon', '2 3', '8', '2 32', '4294967296', 'bucles', 0, FALSE),
    (1,'Nombre de digits', 'Descripcio:Escriu un programa que donat un nombre enter, escrigui el nombre de digits que te.Entrada: Per l''entrada rebreu un nombre enterSortida: La sortida ha de ser “El numero n te x digits”', '123456', 'El numero 123456 te 6 digits', '876365530053454', 'El numero 876365530053454 te 15 digits', 'bucles', 0, FALSE),
    (1,'Coordenades en moviment', 'Descripcio:Feu un programa que llegeixi una sequencia de caracters, cadascun dels quals codificant un moviment (''n'' per nord, ''s'' per sud,''e'' per est, i ''o'' per oest), i que calculi la posicio final d''un objecte que inicialment es trobes a la posicio (0, 0).Suposeu que laprimera component es correspon a la direccio est-oest, que anar cap a l''est significa sumar 1 a aquesta component, i que anarcap al sud significa sumar 1 a la segona component. Quan es rebi una ''z'' es finalitzara el moviment.Entrada: Per l''entrada rebreu una serie de caracters ''n'', ''s'', ''e'' o ''o'' indicant les coordenades de moviment i la sequencia acabara ambuna ''z''Sortida: Cal escriure la posicio final d''un objecte que inicialment es trobes a la posicio (0, 0)', 'n n n n z', '(0, -4)', 's s e o o o o n z', '(-3, 1)', 'bucles', 0, FALSE),
    (1,'Calcula la mitjana', 'Descripcio:Feu un programa que rebi per parametre una sequencia no buida d''enters i calculi la mitjana de tots els valors.Entrada: L''entrada consisteix en una sequencia no buida d''entersSortida: Escriviu el resultat de la mitjana de tots els enters amb decimals', '1 7 3 2 4 7 5 8 7', '4.888888888888889', '-3 10 0 -2 8', '2.6', 'llistes', 0, FALSE),
    (1,'Minim i maxim', 'Descripcio:Feu un programa que rebi per parametre una sequencia no buida d''enters i mostri el minim i el maxim valor dins de lasequencia.Entrada: L''entrada consisteix en una sequencia no buida d''entersSortida: Per la sortida heu d''imprimir el valor minim i maxim de la sequencia', '2 8 19 -32 189 -1 -68 190 0 1', '-68 190', '1 5 6 7 10 19 23 43 198 192 200 0', '0 200', 'llistes', 0, FALSE),
    (1,'Busca el que falta', 'Descripcio:Feu un programa que rebi per parametre una sequencia no buida d''enters i mostri el valor que falta a la sequencia.Entrada: L''entrada consisteix en una sequencia no buida i ordenada d''entersSortida: Per la sortida heu d''imprimir el valor de la sequencia que falta', '1 2 3 4 5 6 7 9 10 11', '8', '4 5 7 8 9', '6', 'llistes', 0, FALSE),
    (1,'Comptant a''s', 'Descripcio:Feu un programa que llegeixi una sequencia de caracters i que escrigui quantes lletres ''a'' conte abans del primer punt.Entrada: L''entrada consisteix en una sequencia de caracters que conte almenys un puntSortida: Cal escriure el nombre de vegades que ''a'' apareix a la sequencia abans del primer punt', 'Jo soc un exemple amb nomes dues as', '2', 'Aqui, una. Si, nomes una', '1', 'llistes', 0, FALSE);
